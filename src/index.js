const get = key => {
  if (Array.isArray(key)) {
    return key.map(selectedKey => {
      try {
        return JSON.parse(window.localStorage.getItem(selectedKey));
      } catch (error) {
        return window.localStorage.getItem(selectedKey);
      }
    });
  } else {
    try {
      return JSON.parse(window.localStorage.getItem(key));
    } catch (error) {
      return window.localStorage.getItem(key);
    }
  }
};

const set = (key, value) => {
  try {
    if (typeof value == "object")
      return window.localStorage.setItem(key, JSON.stringify(value));
    else return window.localStorage.setItem(key, value);
  } catch (error) {
    return window.localStorage.setItem(key, value);
  }
};

const remove = keys => {
  if (Array.isArray(keys)) {
    keys.map(key => {
      window.localStorage.removeItem(key);
    });
  } else {
    window.localStorage.removeItem(keys);
  }
};

const useStorage = ({ useEffect, useState }, key) => {
  const [value, setValue] = useState(get(key));

  useEffect(() => {
    set(key, value);
  }, [value]);

  return [value, setValue];
};

export default { get, set, remove };

export { useStorage };
